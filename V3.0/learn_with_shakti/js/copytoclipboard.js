// copyToClipboard method puts the content passed to this method to the clipboard of the
// host system.
function copyToClipboard(texttocopy){
         var dummy = $('<textarea>').val(texttocopy).appendTo('body').select();
         document.execCommand('copy');
         $(dummy).remove();
        }

function copyTextareaToClipboard(id) {
     console.log(id);
     //select the element with the id "copyMe", must be a text box
     var textToCopy = document.getElementById(id);
     //select the text in the text box
     textToCopy.select();
     //copy the text to the clipboard
     document.execCommand("copy");
}
